import pandas as pd
import numpy as np
import math


class Node:
    children: dict[str, "Node"]
    parent: ("Node" or None)
    X: pd.DataFrame
    Y: pd.Series

    def __init__(self, X=None, Y=None, parent=None, children=[], attr=None, value=None, decision=None):
        self.children = dict().fromkeys(children)
        self.parent = parent
        self.attr = attr
        self.value = value
        self.X = X
        self.Y = Y
        self.decision = decision

    def getattrs(self):
        attrs = set()
        if self.attr is not None:
            attrs.add(self.attr)
        node = self
        while node.parent is not None:
            node = node.parent
            if node.attr is not None:
                attrs.add(node.attr)
        return np.array(attrs)


class DecisionTree:
    def __init__(self):
        self.root = Node()

    def freq(self, X: pd.Series, v):
        count = 0
        for x in X:
            if x == v:
                count += 1
        return count

    def info(self, X: pd.DataFrame, Y: pd.Series):
        sum = 0.0
        for v in Y.unique():
            p = self.freq(Y, v) / float(Y.count())
            sum += p * np.log2(p)
        return -sum

    def infoX(self, X: pd.DataFrame, Y: pd.Series, attr: str):
        sum = 0.0
        for v in X[attr].unique():
            Xi = X[X[attr] == v]
            sum += len(Xi) / len(X) * self.info(Xi, Y[X[attr] == v])
        return sum

    def split(self, X: pd.DataFrame, Y: pd.Series, attr: str):
        sum = 0.0
        for v in X[attr].unique():
            Xi = X[X[attr] == v]
            sum -= len(Xi) / len(X) * np.log2(len(Xi) / len(X))

        return sum

    def gainRatio(self, X: pd.DataFrame, Y: pd.Series, attr: str):
        splitInfoX = self.split(X, Y, attr)
        if abs(splitInfoX) < np.finfo(float).eps:
            return -math.inf
        return (self.info(X, Y) - self.infoX(X, Y, attr)) / splitInfoX

    def getAttrWithMaxGainRatio(self, X: pd.DataFrame, Y: pd.Series, attrs: set[str]):
        maxGainRatio = -math.inf
        attrWithMaxGainRatio = None
        for index, attr in enumerate(list(attrs)):
            gr = self.gainRatio(X, Y, attr)
            if maxGainRatio < gr:
                maxGainRatio = gr
                attrWithMaxGainRatio = attr
        return attrWithMaxGainRatio

    def is_same(self, Y: pd.Series):
        y = Y.to_numpy()
        return (y[0] == y).all()

    def fit(self, X: pd.DataFrame, Y: pd.Series, attrSet: set[str]):
        nodes = [self.root]
        self.root.X = X
        self.root.Y = Y
        i = 0
        while i < len(nodes):
            node = nodes[i]
            i += 1
            x = node.X
            y = node.Y
            if len(y) < 2:
                node.decision = y[0] #new line cinema
                continue
            if y is not None and self.is_same(y):  # testing thing
                node.decision = y[0]
                continue
            attrs = attrSet - node.getattrs()
            resultAttr = self.getAttrWithMaxGainRatio(x, y, attrs)
            if resultAttr is None:
                if y is not None and self.is_same(y): #new line cinema
                    node.decision = y[0]
                else:
                    label_1, label_2 = set(y)
                    max_label = label_1 if np.sum(y == label_1) > np.sum(y == label_2) else label_2
                    node.decision = max_label # end of new line cinema
                continue
            node.attr = resultAttr

            values = x[resultAttr].unique()

            for index, value in enumerate(values):
                rowsWithValue = x[resultAttr] == value
                child = Node(x[rowsWithValue], y[rowsWithValue], node, [], None, value)
                nodes.append(child)
                node.children[value] = child

    def predict(self, X: pd.DataFrame):
        predictions = [self.make_prediction(x, self.root) for i, x in X.iterrows()]
        return predictions

    def make_prediction(self, x: pd.Series, tree: Node):
        if tree.decision is not None: return tree.decision
        val = x[tree.attr]
        if val not in tree.children.keys():
            if self.is_same(tree.Y):
                return tree.Y[0]
            else:
                label_1, label_2 = set(tree.Y)
                max_label = label_1 if np.sum(tree.Y == label_1) > np.sum(tree.Y == label_2) else label_2
                return max_label
        return self.make_prediction(x, tree.children[x[tree.attr]])

    def getRoot(self):
        return self.root


