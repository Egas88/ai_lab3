import pandas as pd
from sklearn.model_selection import train_test_split
from C45 import DecisionTree
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, precision_recall_curve, roc_curve, auc, average_precision_score, \
    precision_score, recall_score, RocCurveDisplay, PrecisionRecallDisplay


df = pd.read_csv("DATA.csv", delimiter=";", index_col="STUDENT ID")


y = df["Succesfull"]
X = df.drop(["Succesfull"], axis=1).sample(n=5, axis=1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

clf = DecisionTree()
attrSet = set(X_train.columns)
clf.fit(X_train, y_train, attrSet)
y_pred = clf.predict(X_test)


accuracy = accuracy_score(y_test, y_pred)
recall = recall_score(y_test, y_pred, average='macro')
precision = precision_score(y_test, y_pred, average='macro')

print(f'accuracy is: {accuracy}')
print(f'recall is: {recall}')
print(f'precision is: {precision}\n')

fpr, tpr, _ = roc_curve(y_test, y_pred)
roc_display = RocCurveDisplay(fpr=fpr, tpr=tpr).plot()
precision, recall, _ = precision_recall_curve(y_test, y_pred)
pr_display = PrecisionRecallDisplay(precision=precision, recall=recall).plot()
auc_roc = auc(fpr, tpr)
auc_pr = average_precision_score(y_test, y_pred)

"""
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 8))
roc_display.plot(ax=ax1)
pr_display.plot(ax=ax2)
"""

print(f'root attribute: {clf.root.attr}')

plt.show()